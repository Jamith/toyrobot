Toy Robot
---------

This is a simulation of toy robot developed in Java 1.8, which can be placed and moved on a
5 units x 5 units square table. Please refer the tech-programming-test-robot-1-0.pdf and
DesignAndArchitecture.md for more details.

Unit Tests
----------

Unit tests are available within /src/test.

Compiling the application
-------------------------

Run the command; mvn compile

Running unit tests
------------------

Run the command; mvn test

Running the application
-----------------------

Run the command; mvn exec:java
This will enable the user to enter command to be given to toy robot.

Packaging the application
-------------------------

Run the command; mvn package
This creates the jar file ./target/ToyRobot-1.0-SNAPSHOT.jar
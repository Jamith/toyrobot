package com.virtual.toyrobot.controller;

import com.virtual.toyrobot.exception.ToyRobotException;
import com.virtual.toyrobot.model.*;
import org.junit.Assert;
import org.junit.Test;

public class GameTest {
    public GameTest() {
    }

    @Test
    public void testPlace() throws ToyRobotException {
        Position origin = new Position(0, 0);
        Position dimension = new Position(5, 5);
        SquareTable squareTable = new SquareTable(origin, dimension);
        Position position = new Position(0, 0);
        Direction direction = Direction.WEST;
        ToyRobot toyRobot = new ToyRobot();
        Game game = new Game(squareTable, toyRobot);
        Assert.assertTrue(game.placeRobot(position, direction));
        Assert.assertTrue(game.getToyRobot().isPresent());
        Assert.assertTrue(game.getToyRobot().get().getPosition().map(p -> p.getX() == 0 && p.getX() == 0).orElse(false));
        Assert.assertTrue(game.getToyRobot().get().getDirection().map(d -> d == Direction.WEST).orElse(false));

    }

    @Test
    public void testProcessInstruction() throws ToyRobotException {
        Position origin = new Position(0, 0);
        Position dimension = new Position(5, 5);
        SquareTable squareTable = new SquareTable(origin, dimension);
        ToyRobot toyRobot = new ToyRobot();
        Game game = new Game(squareTable, toyRobot);
        game.processInstruction("PLACE 0,0,NORTH");
        Assert.assertEquals("0,0,NORTH", game.processInstruction("REPORT"));
        game.processInstruction("MOVE");
        Assert.assertEquals("0,1,NORTH", game.processInstruction("REPORT"));
        game.processInstruction("LEFT");
        Assert.assertEquals("0,1,WEST", game.processInstruction("REPORT"));
        game.processInstruction("RIGHT");
        Assert.assertEquals("0,1,NORTH", game.processInstruction("REPORT"));
    }
}



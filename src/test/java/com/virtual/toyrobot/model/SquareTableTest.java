package com.virtual.toyrobot.model;

import org.junit.Test;
import static org.junit.Assert.*;

public class SquareTableTest {

    public SquareTableTest(){
    }

    @Test
    public void testIsOn() {
        Position validPosition = new Position(0,1);
        Position invalidPosition = new Position(2,7);
        Position origin = new Position(0, 0);
        Position dimension = new Position(5, 5);
        SquareTable squareTable = new SquareTable(origin, dimension);
        assertTrue(squareTable.isOn(validPosition));
        assertFalse(squareTable.isOn(invalidPosition));
    }
}


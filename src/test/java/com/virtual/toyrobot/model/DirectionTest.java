package com.virtual.toyrobot.model;

import org.junit.Assert;
import org.junit.Test;

public class DirectionTest {

    public DirectionTest(){
    }

    /**
     * Test the left direction
     */
    @Test
    public void testLeftDirection() {
        Direction currentDirection = Direction.WEST;
        currentDirection = currentDirection.leftDirection();
        Assert.assertEquals(currentDirection, Direction.SOUTH);
        currentDirection = currentDirection.leftDirection();
        Assert.assertEquals(currentDirection, Direction.EAST);
        currentDirection = currentDirection.leftDirection();
        Assert.assertEquals(currentDirection, Direction.NORTH);
        currentDirection = currentDirection.leftDirection();
        Assert.assertEquals(currentDirection, Direction.WEST);
    }

    /**
     * Test the right direction
     */
    @Test
    public void testRightDirection() {
        Direction currentDirection = Direction.WEST;
        currentDirection = currentDirection.rightDirection();
        Assert.assertEquals(currentDirection, Direction.NORTH);
        currentDirection = currentDirection.rightDirection();
        Assert.assertEquals(currentDirection, Direction.EAST);
        currentDirection = currentDirection.rightDirection();
        Assert.assertEquals(currentDirection, Direction.SOUTH);
        currentDirection = currentDirection.rightDirection();
        Assert.assertEquals(currentDirection, Direction.WEST);
    }
}


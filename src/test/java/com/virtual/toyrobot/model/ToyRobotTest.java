package com.virtual.toyrobot.model;

import org.junit.Assert;
import org.junit.Test;

import java.util.Optional;

public class ToyRobotTest {
    public ToyRobotTest(){
    }

    @Test
    public void testNextPosition(){
        ToyRobot toyRobot = new ToyRobot();
        Assert.assertFalse(toyRobot.nextPosition().isPresent());
        Position position = new Position(0,0);
        Direction direction = Direction.NORTH;
        ToyRobot validToyRobot = new ToyRobot(position, direction);
        Assert.assertTrue(validToyRobot.nextPosition().map(p -> p.getX() == 0 && p.getY() == 1).orElse(false));
        Direction direction1 = Direction.EAST;
        ToyRobot validToyRobot1 = new ToyRobot(position, direction1);
        Assert.assertTrue(validToyRobot1.nextPosition().map(p -> p.getX() == 1 && p.getY() == 0).orElse(false));
        Direction direction2 = Direction.SOUTH;
        ToyRobot validToyRobot2 = new ToyRobot(position, direction2);
        Assert.assertTrue(validToyRobot2.nextPosition().map(p -> p.getX() == 0 && p.getY() == -1).orElse(false));
        Direction direction3 = Direction.WEST;
        ToyRobot validToyRobot3 = new ToyRobot(position, direction3);
        Assert.assertTrue(validToyRobot3.nextPosition().map(p -> p.getX() == -1 && p.getY() == 0).orElse(false));
    }

    @Test
    public void testMove(){
        ToyRobot toyRobot = new ToyRobot();
        Assert.assertFalse(toyRobot.move(true));
        Position position = new Position(0,0);
        Direction direction = Direction.WEST;
        ToyRobot validToyRobot = new ToyRobot(position, direction);
        Assert.assertTrue(validToyRobot.move(true));
        Assert.assertEquals(Optional.of(Direction.WEST), validToyRobot.getDirection());
    }

    @Test
    public void testTurnLeft(){
        Position position = new Position(0,0);
        Direction direction = Direction.WEST;
        ToyRobot validToyRobot = new ToyRobot(position, direction);
        Assert.assertTrue(validToyRobot.turnLeft());
        Assert.assertEquals(Optional.of(Direction.SOUTH), validToyRobot.getDirection());
    }

    @Test
    public void testTurnRight(){
        Position position = new Position(0,0);
        Direction direction = Direction.WEST;
        ToyRobot validToyRobot = new ToyRobot(position, direction);
        Assert.assertTrue(validToyRobot.turnRight());
        Assert.assertEquals(Optional.of(Direction.NORTH), validToyRobot.getDirection());
    }

    @Test
    public void testReport(){
        Position position = new Position(0,0);
        Direction direction = Direction.WEST;
        ToyRobot validToyRobot = new ToyRobot(position, direction);
        Assert.assertFalse(validToyRobot.report().isEmpty());
    }
}
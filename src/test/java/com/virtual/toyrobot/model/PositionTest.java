package com.virtual.toyrobot.model;

import org.junit.Assert;
import org.junit.Test;

public class PositionTest {
    public PositionTest(){
    }

    @Test
    public void testGreaterThanOrEqual() {
        Position position1 = new Position(0,0);
        Position position2 = new Position(4,5);
        Assert.assertTrue(position2.greaterThanOrEqual(position1));
    }

    @Test
    public void testLessThanOrEqual() {
        Position position1 = new Position(0,0);
        Position position2 = new Position(4,5);
        Assert.assertTrue(position1.lessThanOrEqual(position2));
    }

    @Test
    public void testAddCoordinates() {
        Position position1 = new Position(2,3);
        Position position2 = new Position(4,5);
        Position newPosition = position1.addCoordinates(position2);
        Assert.assertEquals(6, newPosition.getX());
        Assert.assertEquals(8, newPosition.getY());
    }
}

package com.virtual.toyrobot.controller;

import com.virtual.toyrobot.exception.ToyRobotException;
import com.virtual.toyrobot.model.*;

import java.util.Optional;
import java.util.regex.Pattern;

public class Game {
    static final Pattern placeInstructionPattern = Pattern.compile("^(PLACE) (\\d),(\\d),(\\w*)$");
    private Optional<Table> squareTable = Optional.empty();
    private Optional<ToyRobot> toyRobot = Optional.empty();

    public Game(Table squareTable, ToyRobot toyRobot) {
        this.squareTable = Optional.ofNullable(squareTable);
        this.toyRobot = Optional.ofNullable(toyRobot);
    }

    public Optional<ToyRobot> getToyRobot() {
        return toyRobot;
    }

    /**
     * placeRobot: Place the robot on the square table
     *
     * @param position:  initial position
     * @param direction: initial direction
     * @return true or false
     * @throws ToyRobotException
     */

    public boolean placeRobot(Position position, Direction direction) throws ToyRobotException {
        if (!this.squareTable.isPresent()) {
            throw ToyRobotException.create("No valid square table object found");
        } else if (!this.toyRobot.isPresent()) {
            throw ToyRobotException.create("No valid toy robot object found");
        } else {
            if (squareTable.get().isOn(position)) {
                this.toyRobot = Optional.of(new ToyRobot(position, direction));
                return true;
            } else {
                return false;
            }
        }

    }

    public String processInstruction(String stringInstruction) throws ToyRobotException {
        String processOutcome;
        try {
            if (this.squareTable.isPresent() && this.toyRobot.isPresent()) {
                if (placeInstructionPattern.matcher(stringInstruction).matches()) {
                    String[] args = stringInstruction.split(" ");
                    String[] params;
                    params = args[1].split(",");
                    int x = Integer.parseInt(params[0]);
                    int y = Integer.parseInt(params[1]);
                    Direction direction = Direction.valueOf(params[2]);
                    this.placeRobot(new Position(x, y), direction);
                    processOutcome = this.toyRobot.map(tr -> tr.report()).orElse("");;
                } else {
                    Instruction instruction = Instruction.valueOf(stringInstruction);
                    switch (instruction) {
                        case MOVE:
                            SquareTable table = (SquareTable) this.squareTable.get();
                            ToyRobot thisRobot = this.toyRobot.get();
                            thisRobot.move(thisRobot.nextPosition().map(np -> table.isOn(np)).orElse(false));
                            processOutcome = thisRobot.report();
                            break;

                        case LEFT:
                            processOutcome = this.toyRobot.map(tr -> {
                                tr.turnLeft();
                                return tr.report();
                            }).orElse("");
                            break;

                        case RIGHT:
                            processOutcome = this.toyRobot.map(tr -> {
                                tr.turnRight();
                                return tr.report();
                            }).orElse("");
                            break;

                        case REPORT:
                            processOutcome = this.toyRobot.map(tr -> tr.report()).orElse("");
                            System.out.println(processOutcome);
                            break;

                        default:
                            throw ToyRobotException.create("Invalid instruction");
                    }
                }
            } else {
                throw ToyRobotException.create("Invalid game. No square table or robot exist");
            }
        } catch (Exception e) {
            throw ToyRobotException.create("Invalid instruction");
        }
        return processOutcome;
    }
}

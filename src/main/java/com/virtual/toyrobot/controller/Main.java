package com.virtual.toyrobot.controller;

import com.virtual.toyrobot.model.Position;
import com.virtual.toyrobot.model.SquareTable;
import com.virtual.toyrobot.model.ToyRobot;
import com.virtual.toyrobot.exception.ToyRobotException;

import java.io.Console;

public class Main {

    public static void main(String[] args) {

        Console console = System.console();

        if (console == null) {
            System.err.println("No console.");
            System.exit(1);
        }

        Position origin = new Position(0, 0);
        Position dimension = new Position(4, 4);
        SquareTable squareBoard = new SquareTable(origin, dimension);
        ToyRobot robot = new ToyRobot();
        Game game = new Game(squareBoard, robot);

        System.out.println("Toy Robot Game");
        System.out.println("Enter a command in the following format:");
        System.out.println("Place the toy robot on the table  -> PLACE X,Y,NORTH|SOUTH|EAST|WEST");
        System.out.println("Move the toy robot by one unit -> LEFT");
        System.out.println("Turn left the toy robot -> LEFT");
        System.out.println("Turn right the toy robot -> RIGHT");
        System.out.println("Report the position and the direction of toy robot -> REPORT");
        System.out.println("Exit the instruction console -> EXIT");

        boolean keepPlaying = true;
        while (keepPlaying) {
            String inputString = console.readLine("$ ");
            if ("EXIT".equals(inputString)) {
                keepPlaying = false;
            } else {
                try {
                    game.processInstruction(inputString);
                } catch (ToyRobotException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }
}


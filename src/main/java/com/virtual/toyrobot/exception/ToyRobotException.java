package com.virtual.toyrobot.exception;

public class ToyRobotException extends Exception{
    public ToyRobotException (String message, Exception fault) {
        super(message, fault);
    }

    public static ToyRobotException create(String message, Exception e) {
        return new ToyRobotException(message, e);
    }

    public static ToyRobotException create(String message) {
        return new ToyRobotException(message, new Exception(message));
    }
}


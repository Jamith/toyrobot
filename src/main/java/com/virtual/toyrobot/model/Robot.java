package com.virtual.toyrobot.model;

public interface Robot {
    boolean move(boolean criteria);
    boolean turnLeft();
    boolean turnRight();
    String report();
}


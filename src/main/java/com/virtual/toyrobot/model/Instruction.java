package com.virtual.toyrobot.model;

public enum Instruction {
    PLACE,
    MOVE,
    LEFT,
    RIGHT,
    REPORT;
}

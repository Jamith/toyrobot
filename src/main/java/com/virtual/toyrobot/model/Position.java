package com.virtual.toyrobot.model;

public class Position{
    private int x;
    private int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    /**
     * greaterThanOrEqual: check whether this position is greater than or equal to in position
     * @param position: position
     * @return
     */
    public boolean greaterThanOrEqual(Position position) {
        return this.x >= position.x && this.y >= position.y;
    }

    /**
     * lessThanOrEqual: check whether this position is less than or equal to in position
     * @param position
     * @return
     */
    public boolean lessThanOrEqual(Position position) {
        return this.x <= position.x && this.y <= position.y;
    }

    /**
     * addCoordinates: Add coordinates of two positions
     * @param position
     * @return
     */
    public Position addCoordinates(Position position) {
        position.x += this.x;
        position.y += this.y;
        return position;
    }

}

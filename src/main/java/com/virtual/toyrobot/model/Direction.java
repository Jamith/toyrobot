package com.virtual.toyrobot.model;

import java.util.HashMap;
import java.util.Map;

public enum Direction {
    NORTH(0),
    EAST(1),
    SOUTH(2),
    WEST(3);

    private static Map<Integer, Direction> directionMap = new HashMap<>();

    static {
        for (Direction directionEnum : Direction.values()) {
            directionMap.put(directionEnum.directionIndex, directionEnum);
        }
    }

    private int directionIndex;

    Direction(int direction) {
        this.directionIndex = direction;
    }

    public static Direction newDirection(int directionNum) {
        return directionMap.get(directionNum);
    }

    /**
     * Returns the rotated left direction
     * @return
     */
    public Direction leftDirection() {
        return rotate(-1);
    }

    /**
     * Returns the rotated right direction
     * @return
     */
    public Direction rightDirection() {
        return rotate(1);
    }

    /**
     * Returns the new direction after rotation
     * @param directionIndicator: step number
     * @return
     */
    private Direction rotate(int directionIndicator) {
        int newIndex;
        if((this.directionIndex + directionIndicator) < 0) {
            newIndex = directionMap.size() - 1;
        } else {
            newIndex = (this.directionIndex + directionIndicator) % directionMap.size();
        }
        return Direction.newDirection(newIndex);
    }

}
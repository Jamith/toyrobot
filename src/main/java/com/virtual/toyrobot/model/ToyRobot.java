package com.virtual.toyrobot.model;

import java.util.Optional;

public class ToyRobot implements Robot {

    private Optional<Position> position = Optional.empty();
    private Optional<Direction> direction = Optional.empty();

    public ToyRobot() {
    }

    public ToyRobot(Position position, Direction direction) {
        this.position = Optional.ofNullable(position);
        this.direction = Optional.ofNullable(direction);
    }

    public void setPosition(Optional<Position> position) {
        this.position = position;
    }

    public Optional<Position> getPosition() {
        return position;
    }

    public Optional<Direction> getDirection() {
        return direction;
    }

    public void setDirection(Optional<Direction> direction) {
        this.direction = direction;
    }

    /**
     * move() : move the robot by one unit forward to the same direction facing
     *
     * @return true if moved successfully otherwise false.
     */

    public boolean move(boolean criteria) {
        boolean condition = criteria && nextPosition().isPresent();
        if(condition){
            this.position = nextPosition();
        }
        return condition;
    }

    /**
     * nextPosition: Returns the possible next position to be moved.
     * @return: Option of Position
     */
    public Optional<Position> nextPosition() {
        Optional<Position> newPosition = Optional.empty();
        if (this.position.isPresent() && this.direction.isPresent()) {
            Position currentPosition = this.position.get();
            Direction currentDirection = this.direction.get();
            switch (currentDirection) {
                case NORTH:
                    newPosition = Optional.of(currentPosition.addCoordinates(new Position(0, 1)));
                    break;
                case EAST:
                    newPosition = Optional.of(currentPosition.addCoordinates(new Position(1, 0)));
                    break;
                case SOUTH:
                    newPosition = Optional.of(currentPosition.addCoordinates(new Position(0, -1)));
                    break;
                case WEST:
                    newPosition = Optional.of(currentPosition.addCoordinates(new Position(-1, 0)));
                    break;
            }
        }
        return newPosition;
    }

    /**
     * turnLeft: Turn the robot left
     *
     * @return outcome
     */

    public boolean turnLeft() {
        return this.direction.map(d -> {
            this.direction = Optional.of(d.leftDirection());
            return true;
        }).orElse(false);
    }

    /**
     * Turn the robot right
     *
     * @return outcome
     */
    public boolean turnRight() {
        return this.direction.map(d -> {
            this.direction = Optional.of(d.rightDirection());
            return true;
        }).orElse(false);
    }

    /**
     * report(): Returns the string of the position and facing direction of the robot.
     * @return
     */
    public String report(){
        StringBuffer reportString = new StringBuffer();
        position.map(p -> reportString.append(p.getX()).append(",").append(p.getY())).orElse(reportString);
        direction.map(d -> reportString.append(",").append(d.toString())).orElse(reportString);
        return reportString.toString();
    }
}


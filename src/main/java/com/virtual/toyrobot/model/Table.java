package com.virtual.toyrobot.model;

public interface Table {
    /**
     * Check whether the given position is on the table
     * @param position: Position
     * @return
     */
    boolean isOn(Position position);
}

package com.virtual.toyrobot.model;

public class SquareTable implements Table {

    private Position origin;
    private Position dimension;

    public SquareTable(Position origin, Position dimension) {
        this.origin = origin;
        this.dimension = dimension;
    }

    /**
     * Check whether the given position is on the table
     * @param position: Position
     * @return
     */
    public boolean isOn(Position position){
        return position.greaterThanOrEqual(this.origin) && position.lessThanOrEqual(this.dimension);
    }
}

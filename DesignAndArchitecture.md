Design and Architecture of Toy Robot
====================================

This document outlines the design and architecture of the implementation.

Robot Functionality
-------------------
 - Robot can only be moved on the square shape table of dimensions 5 units x 5 units.
 - It can only receive move commands only after placing on the table by providing coordination (x,y) to mark the
   initial position on the table.
 - It can move freely on the table space because there are no other obstructions.

Class Hierarchy
---------------
Position - can add and compare coordinates
SquareTable - Accept the origin and dimension positions. Can check whether the given coordinates is on/off the table.
ToyRobot - Can be placed, moved, turned(left/right) and report the current position and facing direction.
Game - Game can accept the Table and ToyRobot.
ToyRobotException - Exception class for ToyRobot application. Consumers of this application need to handle this exception.

Design and Development Methodologies
------------------------------------
Object Oriented Programming(OOP)
Function Programming(FP)
Test Driven Development(TDD) 
Combination of Factory and MVC design patterns.


Instructions
------------
The following instructions are accepted by the toy robot from the command line.
- PLACE X,Y,F
   Placing X and Y coordinate and the facing NORTH, EAST, SOUTH or WEST
- MOVE
  Move 1 unit forward
- LEFT
  Turn left
- RIGHT
  Turn right
- REPORT
  Reports the current position and the facing direction.

Player can give valid instruction to toy robot in String format and it decodes the instruction with internal
format before executing.

Languages and tools used
------------------------
This application has been developed and compiled in Java 8.

Java:
java version "1.8.0_77"
Java(TM) SE Runtime Environment (build 1.8.0_77-b03)
Java HotSpot(TM) 64-Bit Server VM (build 25.77-b03, mixed mode)

Maven 4.0.0

Junit 4.11

Unit Tests
----------
Unit tests are available within /src/test.

Compiling the application
-------------------------
Run the command; mvn compile

Running unit tests
------------------
Run the command; mvn test

Running the application
-----------------------
Run the command; mvn exec:java
This will enable the user to enter command to be given to toy robot.

Packaging the application
-------------------------
Run the command; mvn package
This creates the jar file ./target/ToyRobot-1.0-SNAPSHOT.jar